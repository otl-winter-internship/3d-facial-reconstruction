# 3D Facial Reconstruction

3D facial reconstruction from single image or three images is a challenging and emerging topic in computer vision and computer graphics field.
It is difficult and challenging to reconstruct the 3D facial model from a single photo because of arbitrary poses, non-uniform illumination, expressions, and occlusions.
Detailed 3D facial models are difficult to reconstruct because every algorithm has some limitations related to profile view, fine detail, accuracy, and speed. 
The major problem is to develop 3D face with texture of large poses, wild faces, large training data, and occluded faces.
Mostly algorithms use convolution neural networks and deep learning frameworks to create facial model. 
3D face reconstruction algorithms used for application such as 3D printing, 3D VR games and facial recognition.

# **Citation**
(https://www.igi-global.com/chapter/3d-single-image-face-reconstruction-approaches-with-deep-neural-networks/250758)
(https://www.researchgate.net/publication/335434981_TOWARDS_3D_FACIAL_RECONSTRUCTION_USING_DEEP_NEURAL_NETWORKS)

# **Report # 1**

# 3D Facial Reconstruction

**21/2/2020**

1. I have read research paper which you have given me yesterday named “Deep Face Recognition: A Survey”.
2. Face reconstruction need holistic and local approaches.
3. 3D Morphable Model (3DMM) consists of shape and texture parameter as shown in Fig. 1.

![1](/uploads/62ca2f02db7423171a3aa2c104795e91/1.png)           ![2](/uploads/5f8afd4e408b1fdb53e5962060070181/2.png)
![3](/uploads/eab948a19ed74cfd901adc5da2ed3eec/3.png)

Every 3D Face Reconstruction algorithm consists of following steps:     
1. Crop the image according to the requirements.
2. Detection of landmarks by landmarks detector or manually can also be detect as shown in Figure 2.
3. Input the image and landmarks based model to convolution neural network.
4. Morph the 3DMM Model according to required 3D Model as shown in figure 3.
![4](/uploads/ee089032bab782cb3ce384d96254b27a/4.png)


# **Report # 2**

# 3D Facial Reconstruction

**22/2/2020**

**Setup of Linux**
1. The installation of Ubuntu 16.04 LTS became successful.
2. Internet has made a problem and installation took time.
3. Installed the graphics card driver of NVIDIA GTX 1050.
4. Update and upgrade the Linux system by using command “sudo update && upgrade”.

**Install Python and OpenCV library**
1. I installed OpenCV-3.3.1 setup with libraries by using command “git clone”.
2. After installation, compile the OpenCV library by running the “cmake” command.

**Program Code of Face Detector**
1. I run the face detector program on my face for testing the OpenCV library as shown in the figure 1.
2. Too much error came during the compilation of program as shown in the figure 2.
3. This face detector is not based upon deep neural network. This is simple program which I made on webcam of my laptop.
4. This program is working when face is moving up and down as shown in figure 3. This program needs more improvement by using deep neural networks.

![5](/uploads/9da10ed7bf47ed8589e6db1b717f8f6c/5.png)
![6](/uploads/0de7b795e193a93a1c4479c374aa2e01/6.png)
![7](/uploads/0ee81966a1b91c0f71f3f8f5523d7d51/7.png)
![8](/uploads/7c596d2eb495bd33fde18fd79f8bebd7/8.png)


# **Report # 3**

# 3D Facial Reconstruction

**24/2/2020**

1. Accept the invitation of Gitlab.
2. I have read some research papers, then research papers.

**Research Goal**

Face Detection --> Facial Landmarks Detection --> Prepare Dataset --> Trained the required Model --> Loss Function --> Convolution Neural Network Layers --> 
3D Facial Reconstruction --> Head pose Estimation

**Every Head Pose algorithm consists of following steps:**
1. Crop the image according to the requirements.
2. Detect the face according with the face detector.
3. Detection of landmarks by landmarks detector or manually can also be detect as shown in Figure.
4. Prepare Dataset for Head and face Pose estimation.
5. Input the image and landmarks based model to convolution neural network.
6. Trained the Required Model with the loss function.
7. Morph the 3DMM Model according to required 3D Model.
8. Estimate the Head Pose and align the face.
This is a face alignment and head pose estimation problem. Face alignment across wild and large poses is challenging problem. Face alignment plays an essential role in many 
face related applications such as face recognition, face frontalization and 3D face reconstruction.

Face alignment, also known as facial landmark localization. Landmarks on faces with large pose, occlusion and significant blur are still challenging to localize.Facial landmarks, also known as facial key points or facial feature points, are mainly located around facial components such as eyes, mouth, nose and chin.

![12](/uploads/a0604242eab5e1d6c8d9954fc01a35cd/12.png)
![11](/uploads/4179a986ec29b8a6f71e93e26b57f76d/11.png)
![10](/uploads/c810afdd676c09752a9d5d52c30be7a8/10.png)
![9](/uploads/8a0dacce533eeb6aa0f534910b9ed837/9.png)


# **Report # 4**

# 3D Facial Reconstruction

**25/2/2020**

I have read some research papers as shown in the given below figures and in the references section.
1. Fine-Grained Head Pose Estimation Without Keypoints.
2. Robust and Accurate 3D Head Pose Estimation through 3DMM and Online Head Model Reconstruction
3. FSA-Net: Learning Fine-Grained Structure Aggregation for Head Pose Estimation from a Single Image
4. HeadFusion: 360◦Head Pose tracking combining 3D Morphable Model and 3D Reconstruction

![13](/uploads/f20013890d3f0b926aa56d44784e376d/13.png)![25](/uploads/dc835405b865ccd669e66ca2609a1459/25.png)
![15](/uploads/0647466d5550cc33db7ced7dcab7c29d/15.png)
![14](/uploads/cb6ffb0f4b21ac38cc891a8b7c43a987/14.png)


# **Report # 5**

# 3D Facial Reconstruction

**26/2/2020**

I have read some research papers regarding facial landmark detection, head reconstruction as shown in the given below figure and in the references section.
1. How far are we from solving the 2D & 3D Face Alignment problem? (and a dataset of 230,000 3D facial landmarks).
2. Robust Model-based 3D Head Pose Estimation.
3. Facial Landmark Detection: a Literature Survey.
4. One Millisecond Face Alignment with an Ensemble of Regression Trees.
5. Dlib-ml: A Machine Learning Toolkit.
6. HeadFusion: 360◦Head Pose tracking combining 3D Morphable Model and 3D Reconstruction.

![18](/uploads/e4b6fb04fc0002be97524d7afc432fa1/18.png)
![19](/uploads/6293f126ce3e8caaf3d7ac6243f0ba9b/19.png)
![17](/uploads/0840c927b770e6191f0a46b10f7c3917/17.png)
![16](/uploads/46f34fc08f73ca921b63b4de95f03d3c/16.png)


# **Report # 6**

# 3D Facial Reconstruction 

**27/2/2020**

I have read some research papers regarding CNN and 3D facial reconstruction which is given below in the references section.
1. Face alignment across large poses: A 3d solution.
2. Deep Face Recognition: A Survey

**Experiments and Results**
1. Experiment is conducted on AFLW-3D1 dataset with the algorithm proposed by Zhu.
2. 3D Basel Face Model (BFM) is used which is published by the University of Basel. It has different features i.e.

* 3D Morphable Face Model (Shape and Texture).
* Attribute vectors for gender, height, weight, and age.

3. The output of single image is reconstructed head (Texture & Mesh) with respective poses as shown below in the figure.
4. The profile poses and frontal poses is used as an input and extreme poses need a new dataset which is not open source yet. So, it will take time for the annotation of images.

![24](/uploads/6ca10087f048471fd409ef856d47bee3/24.png)    

# 3D Reconstructed Head Model
![23](/uploads/ca205cccfeb48093e671c4ee01de23a4/23.png)
![22](/uploads/db15242f79b4b4c78ea05c434c9556be/22.png)
![21](/uploads/e2eac08078c20842f7afd5ca3f94920d/21.png)


# **Report # 7**

# 3D Facial Reconstruction 

**28/2/2020**

I have read some research papers regarding fitted head model and 3D facial reconstruction which is given below in the references.
1. Extreme 3D Face Reconstruction: Seeing Through Occlusions.
2. State of the Art on Monocular 3D Face Reconstruction, Tracking, and Applications.

**Experiments and Results**
1. Experiment is conducted on AFLW-3D3 dataset with the algorithm proposed by Zhu.
2. 3D Morphable (3DMM)4 is used which is proposed Volker Blanz and Thomas Vetter. It has different features i.e.

* 3DMM is based on a data set of 3D faces.
* Morphing between faces requires full correspondence between all of the faces.
* Laser scans of 200 heads of young adults (100male and 100 female).
* The resultant faces were represented by approximately 70,000 vertices and the same number of color values.

3. Convolution Neural Network architecture with multiple layer and loss function is used for fitting of 3D model.
4. The output of single image is fitted 3D Model with respective poses as shown below in the figure.
5. The extreme poses as an input image and output will be 3D fitted face model. For more extreme poses, there is need of a new dataset.

![28](/uploads/0c3c34593bd32e626c7d902dc70390a3/28.png)  

# Fitted 3D Head Model
![27](/uploads/63fdae34f630cef22d288fbc9ad44e88/27.png)
![26](/uploads/6f05d7309dd44cf397070f22ee7cb3f7/26.png)

# 3D Reconstructed Head Model
![21](/uploads/e2eac08078c20842f7afd5ca3f94920d/21.png)
![22](/uploads/db15242f79b4b4c78ea05c434c9556be/22.png)
![23](/uploads/ca205cccfeb48093e671c4ee01de23a4/23.png)

# **References**

1. Anh Tuan Tran, Tal Hassner, Iacopo Masi, Eran Paz, Yuval Nirkin, Gerard Medion, Extreme 3D Face Reconstruction: Seeing Through Occlusions, CVPR 2018.
2. M. Zollhöfer J. Thies P. Garrido D. Bradley T. Beeler P. Pérez M. Stamminger M. Nießner C. Theobalt, State of the Art on Monocular 3D Face Reconstruction, 
Tracking, and Applications, Journal of the European Association for Computer Graphics.
3. X Zhu, Z Lei, X Liu, H Shi, SZ Li, Face alignment across large poses: A 3d solution, In Proc. Conf. Comput. Vision Pattern Recognition (CVPR), 2016.
4. Volker Blanz and Thomas Vetter, A Morphable Model for the Synthesis of 3D Faces, SIGGRAPH '99: Proceedings of the 26th annual conference on Computer graphics and 
interactive techniques, July 1999.
6. Mei Wang, Weihong Deng, Deep Face Recognition: A Survey, CVPR 2018.
7. Pascal Paysan, ReinhardKnothe, BrianAmberg, BrianAmberg, Sami Romdhani, Thomas Vetter, A 3D Face Model for Pose and Illumination Invariant Face Recognition, 
Proc. Sixth IEEE international Conference on Advanced Video and Signal Based Surveillance, AVSS 2009. pp. 296-301.
8. Adrian Bulat, Georgios Tzimiropoulos, How far are we from solving the 2D & 3D Face Alignment problem? (and a dataset of 230,000 3D facial landmarks), ICCV 2017.
9. Gregory P. Meyer, Shalini Gupta, Iuri Frosio, Dikpal Reddy, Jan Kautz, Robust Model-based 3D Head Pose Estimation, 2015 IEEE International Conference on Computer Vision.
10. Yue Wu · Qiang Ji, Facial Landmark Detection: a Literature Survey, International Journal on Computer Vision, 2018.
11. Vahid KazemiJosephine Sullivan, One Millisecond Face Alignment with an Ensemble of Regression Trees, CVPR-14: Proceedings of the
2014 IEEE Conference on Computer Vision and Pattern Recognition, June 2014.
12. Davis E. King, Dlib-ml: A Machine Learning Toolkit, Journal of Machine Learning Research 10 (2009) 1755-1758.
13. Yu Yu, Kenneth Alberto Funes Mora, Jean-Marc Odobez, HeadFusion: 360◦Head Pose tracking combining 3D Morphable Model and 3D Reconstruction, Ieee Transactions 
on Pattern Analysis And Machine Intelligence, Vol. 40, No. 11, November 2018.